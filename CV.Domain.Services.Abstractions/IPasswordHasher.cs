﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Services.Abstractions
{
    /// <summary>
    /// Allows to create hashes from passwords.
    /// </summary>
    public interface IPasswordHasher
    {
        /// <summary>
        /// Hashes the specified password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        string Hash(string password);

        /// <summary>
        /// Checks the specified hash.
        /// </summary>
        /// <param name="hash">The hash.</param>
        /// <param name="password">The password.</param>
        /// <returns>True if verified.</returns>
        bool Check(string hash, string password);
    }
}