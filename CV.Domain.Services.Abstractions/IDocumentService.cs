﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CV.Domain.Models;
using LanguageExt;

namespace CV.Domain.Services.Abstractions
{
    /// <summary>
    /// Document processing service.
    /// </summary>
    public interface IDocumentService
    {
        /// <summary>
        /// Retrieves the list of documents.
        /// </summary>
        IEnumerable<Document> Get();

        /// <summary>
        /// Retrieves the document.
        /// </summary>
        Option<Document> GetById(string documentId);

        /// <summary>
        /// Saves the specified documents.
        /// </summary>
        /// <param name="documents">The documents.</param>
        void Save(IList<Document> documents);

        /// <summary>
        /// Updates the specified document.
        /// </summary>
        /// <param name="document">The document.</param>
        ValidationResult Update(string documentId, Document document, string user);

        /// <summary>
        /// Deletes the specified document.
        /// </summary>
        /// <param name="documentId">The document identifier.</param>
        ValidationResult Delete(string documentId, string user);
    }
}
