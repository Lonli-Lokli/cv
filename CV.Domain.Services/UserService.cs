﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CV.Domain.Models;
using CV.Domain.Services.Abstractions;
using CV.Repository.Abstractions;
using LanguageExt;

namespace CV.Domain.Services
{
    /// <inheritdoc />
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHasher _hasher;

        /// <inheritdoc />
        public UserService(IUserRepository userRepository, IPasswordHasher hasher)
        {
            _userRepository = userRepository;
            _hasher = hasher;
        }

        /// <inheritdoc />
        public Either<IReadOnlyCollection<ValidationResult>, User> CreateUser(string userName, string password)
        {
            var validationErrors = new List<ValidationResult>();
            if (string.IsNullOrWhiteSpace(userName))
            {
                validationErrors.Add(new ValidationResult("Name cannot be empty."));
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                validationErrors.Add(new ValidationResult("Password cannot be empty."));
            }

            if (!IsNameAvailable(userName))
            {
                validationErrors.Add(new ValidationResult("Name already taken."));
            }

            // If case of any failures do not create user
            if (validationErrors.Count > 0)
            {
                return validationErrors;
            }

            var passwordHash = _hasher.Hash(password);
            return _userRepository.Save(new User(userName, passwordHash));
        }

        /// <inheritdoc />
        public bool ValidateUserPassword(string userName, string password)
        {
            var user = _userRepository.Get(userName);
            return user != null && _hasher.Check(user.PasswordHash, password);
        }

        /// <inheritdoc />
        public bool IsNameAvailable(string userName)
        {
            return _userRepository.IsNameAvailable(userName);
        }
    }
}
