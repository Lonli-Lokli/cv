﻿// Copyright (c) 2019 under MIT license.

using System;
using System.Security.Claims;
using CV.Domain.Configurations;
using CV.Domain.Services.Abstractions;
using JWT.Algorithms;
using JWT.Builder;

namespace CV.Domain.Services
{
    /// <inheritdoc />
    public class TokenService : ITokenService
    {
        private readonly SecurityOptions _securityOptions;

        /// <inheritdoc />
        public TokenService(SecurityOptions securityOptions)
        {
            _securityOptions = securityOptions;
        }

        /// <inheritdoc />
        public string IssueBearerToken(string userName)
        {
            var token = new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(_securityOptions.Secret)
                .AddClaim("exp", DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds())
                .AddClaim(ClaimTypes.Name, userName)
                .Build();
            return $"Bearer {token}";
        }
    }
}
