﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CV.Domain.Models;
using CV.Domain.Services.Abstractions;
using CV.Repository.Abstractions;
using LanguageExt;

namespace CV.Domain.Services
{
    /// <inheritdoc />
    public class DocumentService : IDocumentService
    {
        private readonly IDocumentRepository _repository;

        /// <inheritdoc />
        public DocumentService(IDocumentRepository repository)
        {
            _repository = repository;
        }

        /// <inheritdoc />
        public IEnumerable<Document> Get()
        {
            return _repository.Get();
        }

        /// <inheritdoc />
        public Option<Document> GetById(string documentId)
        {
            return _repository.GetById(documentId);
        }

        /// <inheritdoc />
        public void Save(IList<Document> documents)
        {
            _repository.Save(documents);
        }

        /// <inheritdoc />
        public ValidationResult Update(string documentId, Document document, string user)
        {
            if (document.Author != user)
            {
                return new ValidationResult("Only author can update documents.");
            }

            if (!_repository.Update(documentId, document))
            {
                return new ValidationResult("Db was not able to update the document.");
            }

            return ValidationResult.Success;
        }

        /// <inheritdoc />
        public ValidationResult Delete(string documentId, string user)
        {
            var document = _repository.GetById(documentId);
            if (document == null)
            {
                return new ValidationResult("Document does not exists");
            }

            if (document.Author != user)
            {
                return new ValidationResult("Only author can delete documents.");
            }

            if (!_repository.Delete(documentId))
            {
                return new ValidationResult("Db was not able to delete the document.");
            }

            return ValidationResult.Success;
        }
    }
}
