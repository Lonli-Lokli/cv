﻿// Copyright (c) 2019 under MIT license.

using System;

namespace CV.Domain.Models
{
    /// <summary>
    /// Document item.
    /// </summary>
    public class Document
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Document"/> class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="author"></param>
        /// <param name="data"></param>
        public Document(string name, string author, byte[] data)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Author = author ?? throw new ArgumentNullException(nameof(author));
            Data = data ?? throw new ArgumentNullException(nameof(data));
        }

        /// <summary>
        /// Represents name of the document.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Represents name of the document.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Author of the document.
        /// </summary>
        public string Author { get; }

        /// <summary>
        /// Document content.
        /// </summary>
        public byte[] Data { get; }
    }
}
