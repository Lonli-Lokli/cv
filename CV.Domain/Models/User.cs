﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Models
{
    /// <summary>
    /// User item.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="passwordHash"></param>
        public User(string name, string passwordHash)
        {
            Name = name;
            PasswordHash = passwordHash;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        /// Gets the password hash.
        /// </summary>
        /// <value>
        /// The password hash.
        /// </value>
        public string PasswordHash { get; }
    }
}
