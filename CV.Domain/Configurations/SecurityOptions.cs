﻿// Copyright (c) 2019 under MIT license.

using System.ComponentModel.DataAnnotations;

namespace CV.Domain.Configurations
{
    /// <summary>
    /// Constants for the Security section.
    /// </summary>
    public static class SecurityConstants
    {
        /// <summary>
        /// The name of Configuration section.
        /// </summary>
        public const string CONFIGURATION_PREFIX = "Security";
    }

    /// <summary>
    /// Security options for the authorization.
    /// </summary>
    public class SecurityOptions : ConfigurationObjectBase
    {
        /// <summary>
        /// Gets or sets the secret used for hashing.
        /// </summary>
        [Required, MinLength(20)]
        public string Secret { get; set; }
    }
}
