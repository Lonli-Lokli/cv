﻿// Copyright (c) 2019 under MIT license.

using System.ComponentModel;

namespace CV.Domain.Configurations
{
    /// <summary>
    /// Constants for the LiteDb.
    /// </summary>
    public static class HashingConstants
    {
        /// <summary>
        /// The name of Configuration section.
        /// </summary>
        public const string CONFIGURATION_PREFIX = "Hashing";
    }

    /// <summary>
    /// Options for the hashing.
    /// </summary>
    public sealed class HashingOptions : ConfigurationObjectBase
    {
        /// <summary>
        /// Gets or sets the iterations during hashing (slower - better).
        /// </summary>
        [DefaultValue(100000)]
        public int Iterations { get; set; } = 100000; // 100ms
    }
}
