﻿// Copyright (c) 2019 under MIT license.

using System.ComponentModel.DataAnnotations;

namespace CV.Domain.Configurations
{
    /// <summary>
    /// Constants for the LiteDb.
    /// </summary>
    public static class LiteDbConfigurationConstants
    {
        /// <summary>
        /// The name of Configuration section.
        /// </summary>
        public const string CONFIGURATION_PREFIX = "LiteDb";
    }

    /// <summary>
    /// Configuration for the LiteDb.
    /// </summary>
    /// <seealso cref="CV.Domain.Configurations.ConfigurationObjectBase" />
    public sealed class LiteDbConfiguration : ConfigurationObjectBase
    {
        /// <summary>
        /// Gets or sets the database path on the disk.
        /// </summary>
        /// <value>
        /// The database path.
        /// </value>
        [Required, MinLength(1)]
        public string DbPath { get; set; }

        /// <summary>
        /// Gets or sets the password required for db.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        [Required, MinLength(1)]
        public string Password { get; set; }
    }
}
