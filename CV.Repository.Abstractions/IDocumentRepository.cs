﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using CV.Domain.Models;

namespace CV.Repository.Abstractions
{
    /// <summary>
    /// Document manager repository
    /// </summary>
    public interface IDocumentRepository
    {
        /// <summary>
        /// Saves the specified documents.
        /// </summary>
        /// <param name="documents">The documents.</param>
        void Save(IList<Document> documents);

        /// <summary>
        /// Gets the document list.
        /// </summary>
        IEnumerable<Document> Get();

        /// <summary>
        /// Gets the document.
        /// </summary>
        /// <param name="id">The identifier.</param>
        Document GetById(string id);

        /// <summary>
        /// Updates the specified document.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns>True if succeeded.</returns>
        bool Update(string documentId, Document document);

        /// <summary>
        /// Deletes the specified document.
        /// </summary>
        /// <param name="documentId">The document identifier.</param>
        /// <returns>True if succeeded.</returns>
        bool Delete(string documentId);
    }
}
