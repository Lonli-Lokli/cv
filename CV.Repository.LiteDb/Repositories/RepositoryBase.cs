﻿// Copyright (c) 2019 under MIT license.

using AutoMapper;
using LiteDB;

namespace CV.Repository.LiteDb.Repositories
{
    /// <summary>
    /// Base repository class.
    /// </summary>
    public class RepositoryBase
    {
        private readonly IDbFactory _dbFactory;

        /// <inheritdoc />
        public RepositoryBase(IDbFactory dbFactory, IMapper mapper)
        {
            _dbFactory = dbFactory;
            Mapper = mapper;
        }

        /// <summary>
        /// Gets the mapper instance.
        /// </summary>
        protected IMapper Mapper { get; }

        /// <summary>
        /// Gets the LiteDb database instance.
        /// </summary>
        /// <returns></returns>
        protected LiteDatabase GetDatabase()
        {
            return _dbFactory.Create();
        }
    }
}
