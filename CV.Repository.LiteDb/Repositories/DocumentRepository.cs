﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CV.Domain.Models;
using CV.Repository.Abstractions;
using CV.Repository.LiteDb.Entities;

namespace CV.Repository.LiteDb.Repositories
{
    /// <inheritdoc />
    public class DocumentRepository : RepositoryBase, IDocumentRepository
    {
        /// <inheritdoc />
        public DocumentRepository(IDbFactory factory, IMapper mapper)
            : base(factory, mapper)
        {
        }

        /// <inheritdoc />
        public void Save(IList<Document> documents)
        {
            var dbItems = Mapper.Map<IEnumerable<DocumentEntity>>(documents);
            using (var db = GetDatabase())
            {
                var docCollection = db.GetCollection<DocumentEntity>();
                docCollection.Insert(dbItems);
            }
        }

        /// <inheritdoc />
        public IEnumerable<Document> Get()
        {
            using (var db = GetDatabase())
            {
                var docCollection = db.GetCollection<DocumentEntity>();
                return docCollection.FindAll().Select(i => i.ToDomain());
            }
        }

        /// <inheritdoc />
        public Document GetById(string id)
        {
            using (var db = GetDatabase())
            {
                var docCollection = db.GetCollection<DocumentEntity>();
                var dbItem = docCollection.FindById(id);
                return dbItem.ToDomain();
            }
        }

        /// <inheritdoc />
        public bool Update(string documentId, Document document)
        {
            var dbItem = Mapper.Map<DocumentEntity>(document);
            dbItem.Id = documentId;
            using (var db = GetDatabase())
            {
                var docCollection = db.GetCollection<DocumentEntity>();
                return docCollection.Update(dbItem);
            }
        }

        /// <inheritdoc />
        public bool Delete(string id)
        {
            using (var db = GetDatabase())
            {
                var docCollection = db.GetCollection<DocumentEntity>();
                return docCollection.Delete(id);
            }
        }
    }
}
