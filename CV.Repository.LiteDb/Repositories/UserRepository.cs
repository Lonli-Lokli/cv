﻿// Copyright (c) 2019 under MIT license.

using AutoMapper;
using CV.Domain.Models;
using CV.Repository.Abstractions;
using CV.Repository.LiteDb.Entities;

namespace CV.Repository.LiteDb.Repositories
{
    /// <inheritdoc />
    public class UserRepository : RepositoryBase, IUserRepository
    {
        /// <inheritdoc />
        public UserRepository(IDbFactory factory, IMapper mapper) : base(factory, mapper)
        {
        }

        /// <inheritdoc />
        public bool IsNameAvailable(string userName)
        {
            using (var db = GetDatabase())
            {
                var users = db.GetCollection<UserEntity>();
                return users.FindOne(u => u.Name == userName) == null;
            }
        }

        /// <inheritdoc />
        public User Save(User user)
        {
            var dbUser = Mapper.Map<UserEntity>(user);
            using (var db = GetDatabase())
            {
                var users = db.GetCollection<UserEntity>();
                users.Insert(dbUser);
                return dbUser.ToDomain();
            }
        }

        /// <inheritdoc />
        public User Get(string userName)
        {
            using (var db = GetDatabase())
            {
                var users = db.GetCollection<UserEntity>();
                return Mapper.Map<User>(users.FindById(userName));
            }
        }
    }
}
