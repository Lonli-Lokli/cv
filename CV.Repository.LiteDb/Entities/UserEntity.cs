﻿// Copyright (c) 2019 under MIT license.

using LiteDB;

namespace CV.Repository.LiteDb.Entities
{
    /// <summary>
    /// LiteDb User entity.
    /// </summary>
    public class UserEntity
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [BsonId]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the password hash.
        /// </summary>
        /// <value>
        /// The password hash.
        /// </value>
        public string PasswordHash { get; set; }
    }
}
