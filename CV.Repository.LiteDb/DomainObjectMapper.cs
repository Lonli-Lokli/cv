﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Models;
using CV.Repository.LiteDb.Entities;

namespace CV.Repository.LiteDb
{
    internal static class DomainObjectMapper
    {
        public static Document ToDomain(this DocumentEntity document) =>
            new Document(document.Name, document.Author, document.Data);

        public static User ToDomain(this UserEntity user) => new User(user.Name, user.PasswordHash);
    }
}