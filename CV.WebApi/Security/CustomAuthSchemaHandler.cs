﻿// Copyright (c) 2019 under MIT license.

using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using CV.Domain.Configurations;
using JWT.Builder;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;

namespace CV.WebApi.Security
{
    internal class CustomAuthSchemaHandler : AuthenticationHandler<CustomAuthSchemaOptions>
    {
        private readonly SecurityOptions _securityOptions;

        /// <inheritdoc />
        public CustomAuthSchemaHandler(
            IOptionsMonitor<CustomAuthSchemaOptions> options,
            SecurityOptions securityOptions,
            ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
            _securityOptions = securityOptions;
        }

        /// <inheritdoc />
        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var handler = this;

            try
            {
                string header = handler.Request.Headers[HeaderNames.Authorization];
                if (string.IsNullOrEmpty(header))
                {
                    return Task.FromResult(AuthenticateResult.NoResult());
                }

                string token = null;

                // extract token from Authorization header
                if (header.StartsWith("bearer ", StringComparison.OrdinalIgnoreCase))
                {
                    token = header.Substring("Bearer ".Length).Trim();
                }

                // No token - unauthorized
                if (string.IsNullOrEmpty(token))
                {
                    return Task.FromResult(AuthenticateResult.NoResult());
                }

                var payload = new JwtBuilder()
                    .WithSecret(_securityOptions.Secret)
                    .MustVerifySignature()
                    .Decode<IDictionary<string, object>>(token);

                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, payload[ClaimTypes.Name].ToString())
                };

                var claimsIdentity = new ClaimsIdentity(claims, CustomAuthSchemaDefaults.AuthenticationScheme);

                return Task.FromResult(AuthenticateResult.Success(new AuthenticationTicket(
                    new ClaimsPrincipal(claimsIdentity),
                    new AuthenticationProperties { AllowRefresh = false },
                    CustomAuthSchemaDefaults.AuthenticationScheme)));
            }
            catch (Exception e)
            {
                return Task.FromResult(AuthenticateResult.Fail(e));
            }
        }

        /// <inheritdoc />
        protected override Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            var handler = this;
            handler.Response.StatusCode = StatusCodes.Status401Unauthorized;
            handler.Response.Headers[HeaderNames.WWWAuthenticate] =
                CustomAuthSchemaDefaults.AuthenticationScheme;

            return handler.Response.WriteAsync("auth failed");
        }
    }
}