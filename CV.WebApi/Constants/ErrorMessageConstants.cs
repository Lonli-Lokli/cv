﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.Constants
{
    /// <summary>
    /// Error message constants
    /// </summary>
    public static class ErrorMessageConstants
    {
        /// <summary>
        /// Error message raised when upload document has been called without attachments.
        /// </summary>
        public const string NoFilesAttached = "No files attached.";

        /// <summary>
        /// Error message raised when update document has been called with attachment count not equal to 1.
        /// </summary>
        public const string OnlyOneFileAllowed = "One and only one file can be updated.";
    }
}
