﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.Constants
{
    /// <summary>
    /// Global Service constants.
    /// </summary>
    public static class ServiceConstants
    {
        /// <summary>
        /// The default controller route.
        /// </summary>
        public const string DefaultControllerRoute = "api/[controller]";
    }
}
