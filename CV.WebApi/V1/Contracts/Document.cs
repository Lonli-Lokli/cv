﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.V1.Contracts
{
    /// <summary>
    /// Document for the UI.
    /// </summary>
    public class Document
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        /// <value>
        /// The author.
        /// </value>
        public string Author { get; set; }
    }
}
