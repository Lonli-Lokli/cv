﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Services.Abstractions;
using CV.WebApi.Constants;
using CV.WebApi.Infrastructure;
using CV.WebApi.V1.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CV.WebApi.V1.Controllers
{
    /// <summary>
    /// Represents a RESTful Users service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route(ServiceConstants.DefaultControllerRoute)]
    [ApiController]
    public class UsersController : BaseController
    {
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService;

        /// <inheritdoc />
        public UsersController(IUserService userService, ITokenService tokenService)
        {
            _userService = userService;
            _tokenService = tokenService;
        }

        /// <summary>
        /// Checks if user name already taken.
        /// </summary>
        /// <response code="200">The user name already taken.</response>
        /// <response code="404">The user name available.</response>
        [HttpGet]
        [Route("exists")]
        public ActionResult CheckUserName([FromQuery] string userName)
        {
            return _userService.IsNameAvailable(userName)
                ? (ActionResult)NotFound()
                : Ok();
        }

        /// <summary>
        /// Creates the user
        /// </summary>
        /// <response code="201">User was successfully created.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPut]
        [ModelStateValidation]
        [AllowAnonymous]
        public IActionResult CreateUser([FromBody]UserCreationRequest creationRequest)
        {
            var result = _userService.CreateUser(creationRequest.Name, creationRequest.Password);
            return result.Match<IActionResult>(right => Ok(_tokenService.IssueBearerToken(right.Name)), BadRequest);
        }

        /// <summary>
        /// Logins the specified user.
        /// </summary>
        /// <param name="loginRequest">The login request.</param>
        /// <response code="200">User passed validation.</response>
        /// <response code="400">Validation failed.</response>
        /// <returns>User token.</returns>
        [HttpPut("login")]
        [ModelStateValidation]
        [AllowAnonymous]
        public ActionResult<string> Login([FromBody] UserLoginRequest loginRequest)
        {
            var result = _userService.ValidateUserPassword(loginRequest.Name, loginRequest.Password);
            return result
                ? (ActionResult<string>)_tokenService.IssueBearerToken(loginRequest.Name)
                : BadRequest();
        }
    }
}
