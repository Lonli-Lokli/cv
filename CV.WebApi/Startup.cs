﻿// Copyright (c) 2019 under MIT license.

using System.IO;
using System.Reflection;
using AutoMapper;
using CV.Domain.Configurations;
using CV.Domain.Services;
using CV.Domain.Services.Abstractions;
using CV.Repository.Abstractions;
using CV.Repository.LiteDb;
using CV.Repository.LiteDb.Repositories;
using CV.WebApi.Extensions;
using CV.WebApi.Infrastructure;
using CV.WebApi.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using static Microsoft.AspNetCore.Mvc.CompatibilityVersion;

namespace CV.WebApi
{
    /// <summary>
    /// Startup class for the application.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services
                .AddAuthentication(CustomAuthSchemaDefaults.AuthenticationScheme)
                .AddScheme<CustomAuthSchemaOptions, CustomAuthSchemaHandler>(CustomAuthSchemaDefaults.AuthenticationScheme, null);
            services
                .AddMvc(mvcOptions =>
                {
                    mvcOptions.EnableEndpointRouting = false;
                    mvcOptions.ModelMetadataDetailsProviders.Add(new RequiredBindingMetadataProvider());
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    mvcOptions.Filters.Add(new AuthorizeFilter(policy));
                })
                .SetCompatibilityVersion(Latest);

            services.AddStandardServices(Configuration, XmlCommentsFilePath);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperDmToDbProfile());
                cfg.AddProfile(new AutoMapperWebApiToDmProfile());
                cfg.AddProfile(new AutoMapperDmToWebApiProfile());
            });
            mapperConfig.AssertConfigurationIsValid();
            var mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            // register validatable settings
            services.AddSetting<LiteDbConfiguration>(
                Configuration.GetSection(LiteDbConfigurationConstants.CONFIGURATION_PREFIX));
            services.AddSetting<HashingOptions>(
                Configuration.GetSection(HashingConstants.CONFIGURATION_PREFIX));
            services.AddSetting<SecurityOptions>(
                Configuration.GetSection(SecurityConstants.CONFIGURATION_PREFIX));

            services.AddSettingsValidatorActuator();

            services.AddSingleton<IDbFactory, DbFactory>();
            services.AddSingleton<IDocumentRepository, DocumentRepository>();
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IPasswordHasher, PasswordHasher>();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IDocumentService, DocumentService>();
            services.AddSingleton<ITokenService, TokenService>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        /// <param name="provider">The provider.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();

            app.UseStandardServices(provider);

            app.UseMvc();
        }

        private static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }
    }
}
