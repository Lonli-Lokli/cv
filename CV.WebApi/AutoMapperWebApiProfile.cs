﻿// Copyright (c) 2019 under MIT license.

using AutoMapper;
using DM = CV.Domain.Models;
using VM = CV.WebApi.V1.Contracts;

namespace CV.WebApi
{
    /// <summary>
    /// API model (contracts) to domain entities map.
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class AutoMapperWebApiToDmProfile : Profile
    {
        /// <summary>
        /// Construct an AutoMapperWebApiProfile object
        /// </summary>
        public AutoMapperWebApiToDmProfile() : this("WebApiToDm")
        {
        }

        /// <summary>
        /// Construct an AutoMapperWebApiToDmProfile object
        /// </summary>
        /// <param name="profileName">Profile Name</param>
        protected AutoMapperWebApiToDmProfile(string profileName) : base(profileName)
        {
        }
    }

    /// <summary>
    /// Domain entities map to API model (contracts).
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class AutoMapperDmToWebApiProfile : Profile
    {
        /// <summary>
        /// Construct an AutoMapperWebApiProfile object
        /// </summary>
        public AutoMapperDmToWebApiProfile() : this("DmToWebApi")
        {
        }

        /// <summary>
        /// Construct an AutoMapperWebApiToDmProfile object
        /// </summary>
        /// <param name="profileName">Profile Name</param>
        protected AutoMapperDmToWebApiProfile(string profileName) : base(profileName)
        {
            CreateMap<DM.Document, VM.Document>();
        }
    }
}
