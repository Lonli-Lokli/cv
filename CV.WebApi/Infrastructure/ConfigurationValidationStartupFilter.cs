﻿// Copyright (c) 2019 under MIT license.

using System;
using System.Collections.Generic;
using CV.Domain.Configurations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace CV.WebApi.Infrastructure
{
    /// <summary>
    /// Class to validate validatable settings on application startup
    /// </summary>
    public class ConfigurationValidationStartupFilter : IStartupFilter
    {
        private readonly IEnumerable<IValidatable> _validatableObjects;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationValidationStartupFilter" /> class.
        /// </summary>
        /// <param name="validatableObjects">The validatable objects.</param>
        public ConfigurationValidationStartupFilter(IEnumerable<IValidatable> validatableObjects)
        {
            _validatableObjects = validatableObjects;
        }

        /// <inheritdoc />
        public Action<IApplicationBuilder> Configure(
            Action<IApplicationBuilder> next)
        {
            foreach (var validatableObject in _validatableObjects)
            {
                validatableObject.Validate();
            }

            return next;
        }
    }
}
