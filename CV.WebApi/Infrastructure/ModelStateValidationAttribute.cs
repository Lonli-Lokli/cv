﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CV.WebApi.Infrastructure
{
    /// <summary>
    /// Attribute for validation of <see cref="Microsoft.AspNetCore.Mvc.ControllerBase.ModelState" />.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Filters.ActionFilterAttribute" />
    public class ModelStateValidationAttribute : ActionFilterAttribute
    {
        /// <inheritdoc />
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context != null)
            {
                if (context.ModelState.IsValid)
                {
                    return;
                }

                context.Result = new BadRequestObjectResult(context.ModelState);
            }
        }
    }
}
